﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshairs : MonoBehaviour
{
    public LayerMask targetMask;
    public Color dotHighlightColour;
    public SpriteRenderer dot;
    public Player player;

    Color dotOriginalColour;

    // Start is called before the first frame update
    void Start()
    {
        dotOriginalColour = dot.color;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * 40 * Time.deltaTime);
    }
    public void detectTarget(Ray ray)
    {
        if (Physics.Raycast(ray, 100, targetMask))
        {
            dot.color = dotHighlightColour;
        }
        else dot.color = dotOriginalColour;

    }
}
