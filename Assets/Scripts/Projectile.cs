﻿using UnityEngine;
using System.Collections;
using System.Collections.Specialized;
using System.Security.Cryptography;

public class Projectile : MonoBehaviour
{

	public LayerMask enemyMask;
	public LayerMask playerMask;
	public LayerMask obstacleMask;
	public LayerMask danmuMask;

	public Color trailColour;

	public ParticleSystem ExplosionEffect;
	public enum ProjectileType { bomb, bullet, mahou};
	public ProjectileType type;

	public AudioClip explodeAudio;
	public float gravity = 9.8f;

	float speed = 10;
	float verticalVelocity;

	public float damage = 1;

	public float lifetime = 3;
	float skinWidth = .1f;

	void Start()
	{
		Destroy(gameObject, lifetime);
		GetComponent<TrailRenderer>().material.SetColor("_TintColor", trailColour);
		verticalVelocity= speed *transform.forward.y;
	}

	public void SetSpeed(float newSpeed)
	{
		speed = newSpeed;
	}

	void Update()
	{
		float moveDistance = speed * Time.deltaTime;
		verticalVelocity -= Time.deltaTime*gravity;
		CheckCollisions(moveDistance);
		transform.position+=new Vector3(transform.forward.x * speed, verticalVelocity, transform.forward.z * speed) * Time.deltaTime;
	}


	void CheckCollisions(float moveDistance)
	{
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit hit;
		float projectile_size = transform.lossyScale.magnitude/3;
		if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, obstacleMask, QueryTriggerInteraction.Collide))
		{
			OnHitObject(hit.point);
			return;
		}

		Collider[] initialCollisions = Physics.OverlapSphere(transform.position, projectile_size, obstacleMask);
		if (initialCollisions.Length > 0||transform.position.y<=transform.lossyScale.y)
		{
			OnHitObject(transform.position);
			return;
		}

		if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, enemyMask, QueryTriggerInteraction.Collide))
		{
			OnHitEnemy(hit.collider, hit.point);
		}
		initialCollisions = Physics.OverlapSphere(transform.position, projectile_size, enemyMask);
		if (initialCollisions.Length > 0)
		{
			OnHitEnemy(initialCollisions[0], transform.position);
		}

		if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, playerMask, QueryTriggerInteraction.Collide))
		{
			OnHitEnemy(hit.collider, hit.point);
		}
		initialCollisions = Physics.OverlapSphere(transform.position, projectile_size, playerMask);
		if (initialCollisions.Length > 0)
		{
			OnHitEnemy(initialCollisions[0], transform.position);
		}

		if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, danmuMask, QueryTriggerInteraction.Collide))
		{
			OnHitDanMu(hit.collider, hit.point);
		}
		initialCollisions = Physics.OverlapSphere(transform.position, projectile_size, danmuMask);
		if (initialCollisions.Length > 0)
		{
			OnHitDanMu(initialCollisions[0], transform.position);
		}
	}
	void OnHitObject(Vector3 hitPoint)
	{
		if (type == ProjectileType.bomb)
		{
			explode(hitPoint);
		}
		if(type!=ProjectileType.mahou)
		Destroy(gameObject);
	}
	void OnHitEnemy(Collider c, Vector3 hitPoint)
	{
		IDamageable damageableObject = c.GetComponent<IDamageable>();
		if (damageableObject != null)
		{
			if (type != ProjectileType.mahou)
				damageableObject.TakeHit(damage, hitPoint, transform.forward);
			else
				damageableObject.TakeHit(damage*Time.deltaTime*5, hitPoint, transform.forward);
		}
		if (type == ProjectileType.bomb)
			explode(hitPoint);
		if (type != ProjectileType.mahou)
			GameObject.Destroy(gameObject);
	}
	void explode(Vector3 hitPoint)
	{
		Destroy(Instantiate(ExplosionEffect.gameObject, hitPoint, Quaternion.identity) as GameObject, ExplosionEffect.startLifetime);
		Collider[] Collisions = Physics.OverlapSphere(transform.position, 1f, enemyMask);
		for (int i = 0; i < Collisions.Length; i++)
		{
			IDamageable damageableObject = Collisions[i].GetComponent<IDamageable>();
			if (damageableObject != null)
			{
				damageableObject.TakeHit(damage, hitPoint, transform.forward);
			}
		}
		Collisions = Physics.OverlapSphere(transform.position, 1f, playerMask);
		for (int i = 0; i < Collisions.Length; i++)
		{
			IDamageable damageableObject = Collisions[i].GetComponent<IDamageable>();
			if (damageableObject != null)
			{
				damageableObject.TakeHit(damage, hitPoint, transform.forward);
			}
		}
		AudioManager.instance.PlaySound(explodeAudio, transform.position);
	}
	void　OnHitDanMu(Collider c, Vector3 hitPoint)
    {
		Projectile danMu = c.GetComponent<Projectile>();
		if(danMu != null)
		{
			if (type == ProjectileType.mahou)
			{
				if (danMu.type == ProjectileType.bomb)
				{
					explode(hitPoint);
					Destroy(c.gameObject);
				}
				Destroy(gameObject);
			}
			
		}
    }
}