﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    Weapon currentWeapon;

    public Transform weaponHold;
    public Weapon startWeapon;
    public bool isBoss = false;
    public string targetTag;
    public void equipWeapon(Weapon Weapon) {
        if (currentWeapon != null)
            Destroy(currentWeapon.gameObject);
        if (weaponHold == null) return;
        if (!isBoss)
        {
            if (Weapon.type == Weapon.WeaponType.cannon)
                weaponHold.localPosition = new Vector3(.8f, .5f, 0);
            else if(Weapon.type == Weapon.WeaponType.mahou|| Weapon.type == Weapon.WeaponType.katana)
                weaponHold.localPosition = new Vector3(1f, 0f, 0);
            else weaponHold.localPosition = new Vector3(.4f, 0f, .8f);
        }
        else
        {
            weaponHold.localPosition = new Vector3(1f, -.1f, .5f);
        }
        currentWeapon = Instantiate(Weapon,weaponHold.position,weaponHold.rotation);
        currentWeapon.transform.parent = weaponHold;
        currentWeapon.isControlled = true;
        if (currentWeapon.animator != null) currentWeapon.animator.enabled = true;

        currentWeapon.targetTag = targetTag;
    }
    void Start()
    {
        if (startWeapon != null)
            equipWeapon(startWeapon);
    }
    public void OnTriggerHold(Vector3 aimPoint)
    {
        currentWeapon.OnTriggerHold( aimPoint);
    }
    public void OnTriggerRelease()
    {
        currentWeapon.OnTriggerRelease();
    }
    public Vector3 recoils()
    {
        if(currentWeapon!=null)
        return currentWeapon.currentRecoils;
        return Vector3.zero;
    }

    public void Reload()
    {
        if (currentWeapon != null)
        {
            currentWeapon.Reload();
        }
    }

    public void Aim(Vector3 aimPoint)
    {
        if (currentWeapon != null)
        {
            currentWeapon.Aim(aimPoint);
        }
    }

    public IEnumerator slowAim(Vector3 aimPoint)
    {
        if (currentWeapon != null)
        {

            Vector3 oldAimPoint = currentWeapon.transform.position + currentWeapon.transform.forward;
            oldAimPoint.y = 0;
            Vector3 currentAimPoint = oldAimPoint;
            float percent = 0;

            while (percent <= 1)
            {
                yield return null;
                percent += Time.deltaTime*2;
                currentAimPoint = Vector3.Lerp(oldAimPoint, aimPoint, percent);
                currentWeapon.Aim(currentAimPoint);
            }
        }
    }
}
