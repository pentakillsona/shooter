﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon
{
    public Transform[] muzzles;
    public Transform shellEjectionPoint;
    public Projectile projectile;
    public Laser laser;
    public Shell shell;
    public float muzzleVelocity=35;
    public float recoils = 2;
    public float reloadTime = .3f;
    public float damage = 4;
    public float aimSpeed = 360;

    public enum FireMode { auto,burst,single};
    public FireMode fireMode;

    public int burstCount;
    public AudioClip shootAudio,reloadAudio;
    MuzzleFlash muzzleFlash;
    int remainingShots;
    bool isReloading,isAttacking=false;
    Vector3 recoilSmoothDampVelocity;
    Vector3 originalPosition;
    private void Start()
    {
        muzzleFlash = GetComponent<MuzzleFlash>();

        remainingShots = burstCount;
        originalPosition = transform.localPosition;
    }
    protected override void Update()
    {
        if (currentRecoils.magnitude < 0.1) currentRecoils = Vector3.zero;
        else currentRecoils *= 0.9f;
        if (!isAttacking)
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, originalPosition, ref recoilSmoothDampVelocity, .1f);
        base.Update();
        if (remainingShots == 0)
        {
            Reload();
        }
    }
    protected override void attack(Vector3 aimPoint)
    {
        if (!isReloading)
        {
            if (fireMode != FireMode.auto)
            {
                remainingShots--;
            }
            if (type == Gun.WeaponType.cannon)
            {
                float dis = Mathf.Max(2, (aimPoint - transform.position).magnitude);
                muzzleVelocity = Mathf.Sqrt((dis - 1f) * 9.8f / 2) * Mathf.Sqrt(2);
            }

            if (type == WeaponType.laser)
                foreach (Transform muzzle in muzzles)
                {
                    Laser newLaser = Instantiate(laser, muzzle.position, muzzle.rotation) as Laser;
                }
            else if (type == WeaponType.mahou)
            {
                mahouAttack( aimPoint);
            }
            else foreach (Transform muzzle in muzzles)
                {
                    Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;
                    newProjectile.SetSpeed(muzzleVelocity);
                }
            
            AudioManager.instance.PlaySound(shootAudio, transform.position);
            if (type == WeaponType.gun)
                Instantiate(shell, shellEjectionPoint.position, shellEjectionPoint.rotation) ;
            if (type == WeaponType.cannon|| type == WeaponType.gun) { 
                muzzleFlash.activate();
                currentRecoils = transform.forward * (-recoils);
                currentRecoils.y = 0;
                transform.localPosition -= Vector3.forward * .2f;
            }
        }
    }

    void mahouAttack(Vector3 aimPoint)
    {
        animator.SetTrigger("attack");

        transform.LookAt(aimPoint + new Vector3(0, transform.position.y, 0));
        foreach (Transform muzzle in muzzles)
        {
            Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;
            newProjectile.SetSpeed(muzzleVelocity);
        }
    }
    public override void Reload()
    {
        if (!isReloading)
        {
            StartCoroutine(AnimateReload());
        }
    }
    IEnumerator AnimateReload()
    {
        isReloading = true;
        yield return new WaitForSeconds(.5f);
        AudioManager.instance.PlaySound(reloadAudio, transform.position);

        float reloadSpeed = 1f / reloadTime;
        float percent = 0;
        Vector3 initialRot = transform.localEulerAngles;
        float maxReloadAngle = 30;

        while (percent < 1)
        {
            percent += Time.deltaTime * reloadSpeed;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            float reloadAngle = Mathf.Lerp(0, maxReloadAngle, interpolation);
            transform.localEulerAngles = initialRot + Vector3.left * reloadAngle;

            yield return null;
        }
        isReloading = false;
        remainingShots = burstCount;
        yield return new WaitForSeconds(.5f);
    }

    public override void Aim(Vector3 aimPoint)
    {
        if (!isReloading && !isAttacking)
        {
            transform.LookAt(aimPoint + new Vector3(0, transform.position.y, 0));
        }
    }
}
