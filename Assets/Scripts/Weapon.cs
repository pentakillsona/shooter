﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum WeaponType { cannon, gun, laser, melee, mahou, katana };
    public WeaponType type;

    public bool isControlled = false;

    public LayerMask playerMask;
    public LayerMask enemyMask;

    public string targetTag;

    public Vector3 currentRecoils = Vector3.zero;
    public float attackSpeed = 10;

    public Animator animator;

    protected bool triggerReleased = true;

    float nextAttackTime;
    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(SelfDestruct());

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (!isControlled)
        {
            transform.Rotate(Vector3.up * 40 * Time.deltaTime);

            Collider[] Collisions = Physics.OverlapSphere(transform.position, .1f, playerMask);
            if (Collisions.Length > 0)
            {
                for (int i = 0; i < Collisions.Length; i++)
                {
                    Player player = Collisions[i].GetComponent<Player>();
                    if (player != null)
                    {
                        player.equipNewWeapon(this);
                        Destroy(gameObject);
                    }
                }
            }
        }
    }
    public void OnTriggerHold(Vector3 aimPoint)
    {
        if (Time.time > nextAttackTime)
        {
            attack(aimPoint);
            nextAttackTime = Time.time + 1 / attackSpeed;
        }
            triggerReleased = false;
    }
    public void OnTriggerRelease()
    {
        triggerReleased = true;
    }
    protected virtual void attack(Vector3 aimPoint) { }
    public virtual void Reload() { }
    public virtual void Aim(Vector3 aimPoint) { }
    IEnumerator SelfDestruct()
    {
        yield return new WaitForSeconds(20f);
        if (!isControlled)
            GameObject.Destroy(gameObject);
    }
}
