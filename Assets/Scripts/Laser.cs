﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {

    private LineRenderer lr;
    public float lifetime = 4, fadetime = 2;

    public float damage = 2;

    public Color initialColour;

    public LayerMask enemyMask;
    public LayerMask playerMask;
    public LayerMask obstacleMask;

    // Use this for initialization
    void Start () {
        lr = GetComponent<LineRenderer>();
        StartCoroutine(Fade());
        lr.SetPosition(0, transform.position);
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 5000, obstacleMask, QueryTriggerInteraction.Collide))
        {
            if (hit.collider)
            {
                lr.SetPosition(1, hit.point);
            }
        }
        else lr.SetPosition(1, transform.forward * 5000);
        CheckCollisions((lr.GetPosition(0)-lr.GetPosition(1)).magnitude);
    }
	
	// Update is called once per frame
	void Update () {
    }

    IEnumerator Fade()
    {
        yield return lifetime;
        Material mat = GetComponent<Renderer>().material;
        float speed = 1 / fadetime;
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            mat.SetColor("_TintColor",  Color.Lerp(initialColour, Color.clear, percent));
            yield return null;
        }
        Destroy(gameObject);
	}


	void CheckCollisions(float moveDistance)
	{
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit[] hit= Physics.RaycastAll(transform.position, transform.forward, moveDistance, enemyMask, QueryTriggerInteraction.Collide);

		for (int i=0;i<hit.Length;i++)
		{
			OnHitEnemy(hit[i].collider, hit[i].point);
		}
	}
	void OnHitEnemy(Collider c, Vector3 hitPoint)
	{
		IDamageable damageableObject = c.GetComponent<IDamageable>();
		if (damageableObject != null)
		{
			damageableObject.TakeHit(damage, hitPoint, transform.forward);
		}
	}
}
