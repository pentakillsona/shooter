﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Bot : LivingEntity
{
    protected UnityEngine.AI.NavMeshAgent pathFinder;
    protected Transform target;
    protected float nextAttackTime;
    float mapSizeX, mapSizeY;

    protected Material skinMaterial;
    protected bool hasTarget = false;
    protected bool isAttacking = false;

    protected WeaponController WeaponController;
    int phase = 1;
    protected Vector3 direction;

    public float refreshInterval = 1;
    public float maxAttackRange = 1.5f, minAttackRange = .5f;
    public float attackSpeed = 1, turnSpeed = 300;
    public float dmg = 1;
    public float speed;

    public Transform warningIcon;
    public Weapon.WeaponType weaponType;
    public bool isBoss = false, isSlow = false;

    protected virtual void Awake()
    {
        pathFinder = GetComponent<UnityEngine.AI.NavMeshAgent>();
        skinMaterial = GetComponent<Renderer>().material;
        WeaponController = GetComponent<WeaponController>();
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (hasTarget)
        {
            nextAttackTime = Time.time + 1 / attackSpeed;
            pathFinder.speed = speed;
        }

        StartCoroutine(updatePath());
        mapSizeX = FindObjectOfType<MapGenerator>().currentMapSizeX;
        mapSizeY = FindObjectOfType<MapGenerator>().currentMapSizeY;

        skinMaterial.color = originalColour;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (target == null) return;

        if ((target.position - transform.position).sqrMagnitude <= maxAttackRange * maxAttackRange)
        {

            if (Time.time > nextAttackTime && !isAttacking)
            {
                if ((weaponType == Weapon.WeaponType.gun|| weaponType == Weapon.WeaponType.katana) && target != null)
                {
                    Ray ray = new Ray(transform.position, target.position - transform.position);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, (target.position - transform.position).magnitude, obstacleMask, QueryTriggerInteraction.Collide))
                    {
                        pathFinder.enabled = true;
                        return;
                    }

                    if (Physics.Raycast(ray, out hit, (target.position - transform.position).magnitude, teamMask, QueryTriggerInteraction.Collide))
                    {
                        pathFinder.enabled = true;
                        return;
                    }
                    pathFinder.enabled = false;
                }
                if (weaponType == Weapon.WeaponType.gun && pathFinder.enabled) return;
                else
                {
                    pathFinder.enabled = false;
                    StartCoroutine(Attack());
                }
            }
        }
        else pathFinder.enabled = true;
    }
    void FixedUpdate()
    {
        if (weaponType != Weapon.WeaponType.melee && !pathFinder.enabled && !isAttacking)
        {
            Vector3 velocity = speed * direction;
            Vector3 newPos = transform.position + (velocity) * Time.fixedDeltaTime;
            newPos.x = Mathf.Clamp(newPos.x, (-mapSizeX + transform.lossyScale.x) / 2, (mapSizeX - transform.lossyScale.x) / 2);
            newPos.z = Mathf.Clamp(newPos.z, (-mapSizeY + transform.lossyScale.z) / 2, (mapSizeY - transform.lossyScale.z) / 2);
            transform.position = newPos;

            if (target != null)
            {
                Vector3 toDirection = target.position - transform.position;
                toDirection.y = 0;
                Quaternion toRotation = Quaternion.FromToRotation(Vector3.forward, toDirection);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, Time.deltaTime * turnSpeed);
            }
        }
    }

    protected virtual IEnumerator Attack()
    {
        direction = new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)).normalized;
        isAttacking = true;
        if (weaponType == Weapon.WeaponType.melee)
        {
            skinMaterial.color = Color.red;

            Vector3 originalPos = transform.position;
            Vector3 attackPos = target.position;
            yield return new WaitForSeconds(1 / attackSpeed / 2);
            float percent = 0;
            bool attacked = false;

            while (percent <= 1)
            {
                yield return null;
                percent += Time.deltaTime * attackSpeed * 3;
                float interpolation = (-percent * percent + percent) * 4;
                transform.position = Vector3.Lerp(originalPos, attackPos, interpolation);

                if (target != null && !attacked && (target.position - transform.position).sqrMagnitude <= (target.lossyScale + transform.lossyScale).sqrMagnitude / 12)
                {
                    attacked = true;
                    target.GetComponent<IDamageable>().TakeHit(dmg, target.position, transform.forward);
                }
            }

            skinMaterial.color = originalColour;
        }
        else if (weaponType == Weapon.WeaponType.cannon || weaponType == Weapon.WeaponType.gun)
        {
            Vector3 hitPoint = target.position;
            hitPoint += new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f));
            if ((new Vector2(hitPoint.x, hitPoint.z) - new Vector2(WeaponController.transform.position.x, WeaponController.transform.position.z)).magnitude < minAttackRange)
                hitPoint += transform.forward * minAttackRange;
            if (isSlow)
            {
                StartCoroutine(WeaponController.slowAim(hitPoint - new Vector3(0, hitPoint.y, 0)));

                yield return new WaitForSeconds(1 / attackSpeed / 2);
            }
            else WeaponController.Aim(hitPoint - new Vector3(0, hitPoint.y, 0));
            WeaponController.OnTriggerHold(hitPoint);
            WeaponController.OnTriggerRelease();
            if (weaponType == Weapon.WeaponType.cannon)
            {
                GameObject p = Instantiate(warningIcon.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, Vector3.down)) as GameObject;
                Destroy(p, 1);
            }

        }
        else if (weaponType == Weapon.WeaponType.katana)
        {
            skinMaterial.color = Color.red;

            Vector3 originalPos = transform.position;
            Vector3 hitPoint = target.position;
            pathFinder.enabled = true;
            yield return new WaitForSeconds(1 / attackSpeed / 2);
            pathFinder.enabled = false;
            WeaponController.OnTriggerHold(hitPoint);
            yield return new WaitForSeconds(1 / attackSpeed / 2);
            WeaponController.OnTriggerRelease();

            skinMaterial.color = originalColour;
        }
        nextAttackTime = Time.time + 1 / attackSpeed;
        isAttacking = false;
    }

    IEnumerator updatePath()
    {
        while (  !dead)
        {
            if (pathFinder.enabled&& target != null)
                pathFinder.SetDestination(target.position);
            yield return new WaitForSeconds(refreshInterval);
        }
    }
    public override void TakeHit(float dmg, Vector3 hitPoint, Vector3 hitDir)
    {
        base.TakeHit(dmg, hitPoint, hitDir);
        if (isBoss && health < startingHealth * 2 / 3 && phase == 1)
        {
            LevelSpawner spawner = FindObjectOfType<LevelSpawner>();
            spawner.NextWave();
            phase++;
        }
    }
}
