﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour, IDamageable
{
    protected float health;
    public float startingHealth = 2;
    public Transform healthBar;

    protected bool dead = false;
    protected Rigidbody myRigidbody;

    public event System.Action OnDeath;
    public ParticleSystem hitEffect;

    public Color originalColour;

    protected LayerMask teamMask;
    protected LayerMask playerMask;
    protected LayerMask obstacleMask;

    public bool paused = false;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        health = startingHealth;
        myRigidbody = GetComponent<Rigidbody>();
        playerMask = LayerMask.GetMask("player");
        obstacleMask = LayerMask.GetMask("obstacle");
    }
    public virtual void TakeHit(float dmg, Vector3 hitPoint, Vector3 hitDir)
    {
        health -= dmg;
        var em = hitEffect.emission;
        em.enabled = true;

        em.type = ParticleSystemEmissionType.Time;

        em.SetBursts(
            new ParticleSystem.Burst[]{
                new ParticleSystem.Burst(.0f, 10*dmg)
            });
        hitEffect.startColor = originalColour;
        GameObject p = Instantiate(hitEffect.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDir)) as GameObject;
        Destroy(p, hitEffect.startLifetime);
    }

    protected void die()
    {
        Destroy(gameObject);
        dead = true;
        if (OnDeath != null)
            OnDeath();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if ((health <= 0 || transform.position.y < 0) && !dead) die();
        float healthPercent = health / startingHealth;
        healthBar.localScale = new Vector3(healthPercent, .1f, .1f);
        healthBar.localPosition = new Vector3(-(1 - healthPercent) / 2, 0, 0);
    }
}
