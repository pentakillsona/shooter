﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    void TakeHit(float dmg, Vector3 hitPoint, Vector3 hitDir);
}