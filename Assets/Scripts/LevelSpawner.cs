﻿using UnityEngine;
using System.Collections;

public class LevelSpawner : MonoBehaviour
{
	public Level[] levels;

	LivingEntity playerEntity;
	Transform playerT;
	public LivingEntity player;

	Wave currentWave;
	Level currentLevel;
	int currentWaveNumber;
	public int currentLevelNumber = 4;
	int currentTypeNumber = 1;
	int wavesRemainingToSpawn;

	int enemiesRemainingToSpawn;
	int enemiesRemainingAlive;
	int enemiesCurrentlyAlive = 0;
	float nextSpawnTime;

	MapGenerator map;

	float timeBetweenCampingChecks = 2;
	float campThresholdDistance = 1.5f;
	float nextCampCheckTime;
	Vector3 campPositionOld;
	bool isCamping;

	bool isDisabled;

	public event System.Action<int> OnNewWave;
	public event System.Action<int> OnNewLevel;

	public event System.Action OnClear;

	void Start()
	{

		nextCampCheckTime = timeBetweenCampingChecks + Time.time;

		map = FindObjectOfType<MapGenerator>();

		for (int i = 0; i < levels.Length; i++)
			for (int j = 0; j < levels[i].waves.Length; j++)
				levels[i].enemyCount += levels[i].waves[j].enemyCount;

		NextLevel();
	}

	void Update()
	{
		if (!isDisabled)
		{
			if (Time.time > nextCampCheckTime)
			{
				nextCampCheckTime = Time.time + timeBetweenCampingChecks;

				isCamping = (Vector3.Distance(playerT.position, campPositionOld) < campThresholdDistance);
				campPositionOld = playerT.position;
			}
			if ((enemiesRemainingToSpawn > 0 || currentWave.infinite) && (Time.time > nextSpawnTime || enemiesCurrentlyAlive == 0))
			{
				enemiesRemainingToSpawn--;
				nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;
				enemiesCurrentlyAlive++;

				currentTypeNumber = (1 + currentTypeNumber) % currentWave.enemyTypes.Length;
				StartCoroutine(SpawnEnemy(currentTypeNumber));
			}
		}
	}

	IEnumerator SpawnEnemy(int type)
	{
		float spawnDelay = 1;
		float tileFlashSpeed = 4;

		Transform spawnTile = map.GetRandomOpenTile();
		Material tileMat = spawnTile.GetComponent<Renderer>().material;
		Color initialColour = tileMat.color;
		Color flashColour = Color.red;
		float spawnTimer = 0;

		while (spawnTimer < spawnDelay)
		{

			tileMat.color = Color.Lerp(initialColour, flashColour, Mathf.PingPong(spawnTimer * tileFlashSpeed, 1));

			spawnTimer += Time.deltaTime;
			yield return null;
		}

		Enemy spawnedEnemy = Instantiate(currentWave.enemyTypes[type], spawnTile.position + Vector3.up, Quaternion.identity) as Enemy;
		spawnedEnemy.OnDeath += OnEnemyDeath;
	}

	void OnPlayerDeath()
	{
		isDisabled = true;
	}

	void OnEnemyDeath()
	{
		enemiesRemainingAlive--;
		enemiesCurrentlyAlive--;
		if (enemiesRemainingAlive == 0)
		{
			NextWave();
		}
	}

	void ResetPlayerPosition()
	{
		playerT.position = map.GetTileFromPosition(Vector3.zero).position + Vector3.up * 3;
	}

	public void NextWave()
	{
		currentWaveNumber++;

		if (currentWaveNumber - 1 < currentLevel.waves.Length)
		{
			currentWave = currentLevel.waves[currentWaveNumber - 1];

			enemiesRemainingToSpawn = currentWave.enemyCount;
			enemiesRemainingAlive += enemiesRemainingToSpawn;

			if (OnNewWave != null)
			{
				OnNewWave(currentWaveNumber);
			}
		}
		else if (enemiesRemainingAlive == 0) OnClear();
	}

	public void NextLevel()
	{
		currentLevelNumber++;
		loadLevel();
	}

	public void loadLevel()
	{
		if (playerEntity == null)
		{
			playerEntity = Instantiate(player, Vector3.zero + Vector3.up, Quaternion.identity) as LivingEntity;
			playerT = playerEntity.transform;
			campPositionOld = playerT.position;
			playerEntity.OnDeath += OnPlayerDeath;
		}

		currentWaveNumber = 0;

		if (currentLevelNumber - 1 < levels.Length)
		{
			currentLevel = levels[currentLevelNumber - 1];

			wavesRemainingToSpawn = currentLevel.waves.Length;

			if (OnNewLevel != null)
			{
				OnNewLevel(currentLevelNumber);
			}
			ResetPlayerPosition();

			if (OnNewLevel != null)
				OnNewLevel(currentLevelNumber);
			isDisabled = false;
			enemiesRemainingAlive = 0;
			enemiesCurrentlyAlive = 0;

			NextWave();
		}
		else OnClear();
	}

	[System.Serializable]
	public class Wave
	{
		public int enemyCount;
		public float timeBetweenSpawns;
		public bool infinite;
		public Enemy[] enemyTypes;
	}

	[System.Serializable]
	public class Level
	{
		[HideInInspector] public int enemyCount = 0;

		public Wave[] waves;
	}

}