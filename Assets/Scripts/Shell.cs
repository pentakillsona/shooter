﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    Rigidbody r;
    public float forceMin,forceMax;
    public float lifetime = 4, fadetime = 2;

    // Start is called before the first frame update
    void Start()
    {
        float force = Random.Range(forceMin, forceMax);
        r = GetComponent<Rigidbody>();
        r.AddForce(transform.right * force);
        r.AddTorque(Random.insideUnitSphere * force);
        StartCoroutine( Fade());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Fade()
    {
        yield return lifetime;
        Material mat = GetComponent<Renderer>().material;
        Color initialColour = mat.color;
        float speed = 1 / fadetime;
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            mat.color = Color.Lerp(initialColour, Color.clear, percent);
            yield return null;
        }
        Destroy(gameObject);
    }
}
