﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    public GameObject flash;
    public float flashTime;
    public Sprite[] flashSprites;
    public SpriteRenderer[] flashSpritesRenderers;
    public void activate()
    {
        flash.SetActive(true);
        int index = Random.Range(0, flashSprites.Length);
        for (int i = 0; i < flashSpritesRenderers.Length; i++)
            flashSpritesRenderers[i].sprite = flashSprites[index];
        Invoke("deactivate", flashTime);
    }
    void deactivate()
    {
        flash.SetActive(false);
    }
    private void Start()
    {
        deactivate();
    }
}
