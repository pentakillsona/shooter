﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {
	public GameObject gameoverScreen;
	public GameObject winScreen;
	public GameObject optionsMenuHolder;

	public Image fadePlane;
	public RectTransform newWaveBanner;
	public Text newLevelTitle, newLevelEnemy;

	public AudioClip winTheme, mainTheme;

	bool isGameover;
	bool paused = false;
	LevelSpawner spawner;
	Vector2 newWaveBannerPos;
	LivingEntity player;

	private void Awake()
	{
		spawner = FindObjectOfType<LevelSpawner>();
		spawner.OnClear += OnGameWon;
		spawner.OnNewWave += OnNewWave;
		spawner.OnNewLevel += OnNewLevel;
		newWaveBannerPos = newWaveBanner.position;
	}
	// Use this for initialization
	void Start () {
		ContinueGame();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (!paused)
			{
				PauseGame();
			}
			else if (paused)
			{
				ContinueGame();
			}
		}
	}
	public void playAgain()
	{
		gameoverScreen.SetActive(false);
		isGameover = false;
		StartCoroutine(Fade(Color.clear, Color.clear, .1f));
		Cursor.visible = false;
		spawner.loadLevel();
	}
	public void nextLevel()
	{
		winScreen.SetActive(false);
		isGameover = false;
		StartCoroutine(Fade(Color.clear, Color.clear, 1f));
		Cursor.visible = false;
		spawner.NextLevel();
	}
	void OnGameover(){
		if (isGameover) return;
		gameoverScreen.SetActive (true);
		isGameover = true;
		StartCoroutine(Fade(Color.clear, Color.white, .1f));
		Cursor.visible = true;
	}
	void OnGameWon()
	{
		if (isGameover) return;
		winScreen.SetActive(true);
		isGameover = true;
		StartCoroutine(Fade(Color.clear, Color.white, .1f));
		Cursor.visible = true;
	}

	IEnumerator Fade(Color from, Color to, float time)
	{
		float speed = 1 / time;
		float percent = 0;

		while (percent < 0.8)
		{
			fadePlane.color = Color.Lerp(from, to, percent);
			yield return null;
			percent += Time.deltaTime * speed;
		}
	}
	void OnNewWave(int currentWaveNumber)
	{
	}
	void OnNewLevel(int currentLevelNumber)
	{
		newLevelTitle.text = "- Level " + currentLevelNumber + " -";
		newLevelEnemy.text = "Enemies: " + spawner.levels[currentLevelNumber - 1].enemyCount;
		StartCoroutine(AnimateNewLevelBanner());

		player = FindObjectOfType<Player>();
		if (player == null) player = FindObjectOfType<Ally>();
		if (player!=null)
		player.OnDeath += OnGameover;
	}
	IEnumerator AnimateNewLevelBanner()
	{

		float delayTime = 1.5f;
		float speed = 3f;
		float animatePercent = 0;
		int dir = 1;

		float endDelayTime = Time.time + 1 / speed + delayTime;

		while (animatePercent >= 0)
		{
			animatePercent += Time.deltaTime * speed * dir;

			if (animatePercent >= 1)
			{
				animatePercent = 1;
				if (Time.time > endDelayTime)
				{
					dir = -1;
				}
			}

			newWaveBanner.anchoredPosition = Vector2.up * Mathf.Lerp(-170, 45, animatePercent)+newWaveBannerPos;
			yield return null;
		}

	}
	private void PauseGame()
	{
		if (isGameover) return;
		Time.timeScale = 0;
		paused=true;
		player.paused = true;
		//Disable scripts that still work while timescale is set to 0
		optionsMenuHolder.SetActive(true);
		fadePlane.color = Color.Lerp(Color.clear, Color.white, .8f);
	}
	private void ContinueGame()
	{
		if (isGameover) return;
		Time.timeScale = 1;
		paused=false;
		if(player!=null)
		player.paused = false;
		//enable the scripts again
		Cursor.visible = false;
		optionsMenuHolder.SetActive(false);
		fadePlane.color = Color.clear;
	}
}
