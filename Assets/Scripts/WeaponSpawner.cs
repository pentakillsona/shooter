﻿using UnityEngine;
using System.Collections;

public class WeaponSpawner : MonoBehaviour
{
	public Weapon[] WeaponTypes;

	LivingEntity playerEntity;
	Transform playerT;

	float nextSpawnTime;

	MapGenerator map;

	public float timeBetweenWeapons = 2;
	float nextWeaponTime;

	void Start()
	{
		playerEntity = FindObjectOfType<Player>();
		if(playerEntity!=null)
		playerT = playerEntity.transform;

		nextWeaponTime = timeBetweenWeapons + Time.time;

		map = FindObjectOfType<MapGenerator>();
	}

	void Update()
	{
			if (Time.time > nextWeaponTime)
			{
				nextWeaponTime = Time.time + timeBetweenWeapons;
				StartCoroutine(SpawnWeapon(Random.Range(0,WeaponTypes.Length)));
			}
	}

	IEnumerator SpawnWeapon(int type)
	{
		float spawnDelay = 1;
		float tileFlashSpeed = 4;

		Transform spawnTile = map.GetRandomOpenTile();
		Vector3 spawnPosition = spawnTile.position;
		Material tileMat = spawnTile.GetComponent<Renderer>().material;
		Color initialColour = tileMat.color;
		Color flashColour = Color.green;
		float spawnTimer = 0;

		while (spawnTimer < spawnDelay)
		{

			tileMat.color = Color.Lerp(initialColour, flashColour, Mathf.PingPong(spawnTimer * tileFlashSpeed, 1));

			spawnTimer += Time.deltaTime;
			yield return null;
		}

		Weapon spawnedWeapon = Instantiate(WeaponTypes[type], spawnPosition + Vector3.up, Quaternion.identity) as Weapon;
	}

}