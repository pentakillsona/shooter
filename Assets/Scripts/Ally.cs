﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ally : Bot
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        teamMask= LayerMask.GetMask("player");
        FindObjectOfType<LevelSpawner>().OnNewLevel += OnNewLevel;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (target == null)
        {

            GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
            if (enemy == null) return;

            target = enemy.transform;
            hasTarget = true;
        }
        base.Update();
    }

    void OnNewLevel(int currentLevelNumber)
    {
        if (this != null)
        {
            base.Start();
        }
    }
}
