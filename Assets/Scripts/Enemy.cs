﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(Renderer))]
public class Enemy : Bot
{

    protected override void Awake()
    {
        base.Awake();

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null) return;

        target = player.transform;
        hasTarget = true;
    }
    protected override void Start()
    {
        base.Start();
        teamMask = LayerMask.GetMask("enemy");
        FindObjectOfType<LevelSpawner>().OnNewLevel += OnNewLevel;
    }

    void OnNewLevel(int currentLevelNumber)
    {
        if (this != null)
            Destroy(gameObject);
    }
}
