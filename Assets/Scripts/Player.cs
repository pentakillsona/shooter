﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(WeaponController))]
public class Player : LivingEntity {
	Camera viewCam;
	public WeaponController WeaponController;

	public float speed=6;
	public float turnSpeed=8;
	public float smoothMoveTime=0.1f;
	public float mapSizeX, mapSizeY;

	Crosshairs crosshairs;

	Vector3 direction;
	float angle;

	private void Awake()
	{
		viewCam = Camera.main;
		WeaponController = GetComponent<WeaponController>();
		FindObjectOfType<LevelSpawner>().OnNewLevel += OnNewLevel;
		crosshairs = FindObjectOfType<Crosshairs>();

		AudioManager.instance.playerT= FindObjectOfType<Player>().transform;
	}
	protected override void Start(){
		base.Start(); 
		teamMask = LayerMask.GetMask("player");
	}
	// Update is called once per frame
	protected override void Update () {
		if (paused) return;
		base.Update();
		Vector3 inputDir = new Vector3 (Input.GetAxisRaw("Horizontal"),0,Input.GetAxisRaw("Vertical"));
		direction = inputDir.normalized;

		float targetAngle = Mathf.Atan2 (direction.x, direction.z) * Mathf.Rad2Deg;
		angle = Mathf.LerpAngle (angle, targetAngle, Time.deltaTime * turnSpeed);

		Ray ray = viewCam.ScreenPointToRay(Input.mousePosition);
		Plane ground = new Plane(Vector3.up, Vector3.zero);
		float rayDis;
		Vector3 point=Vector3.zero;
		if (ground.Raycast(ray, out rayDis))
		{
			point = ray.GetPoint(rayDis);
			transform.LookAt(new Vector3(point.x,transform.position.y,point.z));
			crosshairs.transform.position = point;
			crosshairs.detectTarget(ray);
			if ((new Vector2(point.x, point.z) - new Vector2(transform.position.x, transform.position.z)).sqrMagnitude > 1)
			{
				WeaponController.Aim(point);
			}
		}

		if (Input.GetMouseButton(0))
			WeaponController.OnTriggerHold(point);
		else if (Input.GetMouseButtonUp(0)) WeaponController.OnTriggerRelease();
		if (Input.GetKeyDown(KeyCode.R)) WeaponController.Reload();
	}
	void FixedUpdate(){
		Vector3 velocity = speed * direction + WeaponController.recoils();
		Vector3 newPos= myRigidbody.position + (velocity) * Time.fixedDeltaTime;
		newPos.x = Mathf.Clamp(newPos.x, (-mapSizeX + transform.lossyScale.x) / 2, (mapSizeX - transform.lossyScale.x) / 2 );
		newPos.z = Mathf.Clamp(newPos.z, (-mapSizeY + transform.lossyScale.z) / 2 , (mapSizeY - transform.lossyScale.z) / 2 );
		myRigidbody.position= newPos;
	}
	void OnNewLevel(int currentLevelNumber)
	{
		health = startingHealth;
		mapSizeX = FindObjectOfType<MapGenerator>().currentMapSizeX;
		mapSizeY = FindObjectOfType<MapGenerator>().currentMapSizeY;
	}
	public void equipNewWeapon(Weapon Weapon)
	{
		WeaponController.equipWeapon(Weapon);
	}
}
