﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{

	public AudioClip mainTheme;
	public AudioClip menuTheme;

	string sceneName;

	void Start()
	{
			OnNewScene("Menu");
	}


	public void OnNewScene(string newSceneName)
	{
		if (newSceneName != sceneName)
		{
			sceneName = newSceneName;
			PlayMusic();
		}
	}

	void PlayMusic()
	{
		AudioClip clipToPlay = null;

		if (sceneName == "Menu")
		{
			clipToPlay = menuTheme;
		}
		else if (sceneName == "Game")
		{
			clipToPlay = mainTheme;
		}

		if (clipToPlay != null)
		{
			AudioManager.instance.PlayMusic(clipToPlay, true);
		}

	}

}