﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility 
{
    public static T[] ShuffleArray<T>(T[] array,int seed=0)
	{
		System.Random rng = new System.Random(seed);
		for (int i = 0; i < array.Length - 1; i++)
		{
			int randomIndex = rng.Next(i, array.Length);
			T v = array[i];
			array[i] = array[randomIndex];
			array[randomIndex] = v;
		}
		return array;
	}
}
