﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Weapon
{
    public float damage = 4;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }
    protected override void attack(Vector3 aimPoint)
    {
        animator.SetTrigger("attack");
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == targetTag)
        {
            col.GetComponent<LivingEntity>().TakeHit(damage, col.transform.position, col.transform.position - transform.position);
        }
    }
}