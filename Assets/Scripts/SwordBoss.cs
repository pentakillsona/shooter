﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordBoss : Enemy
{
    
    protected override IEnumerator Attack()
    {
        direction = new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)).normalized;
        isAttacking = true;
        skinMaterial.color = Color.red;

        Vector3 originalPos = transform.position;
        Vector3 hitPoint = target.position;
        pathFinder.enabled = true;
        yield return new WaitForSeconds(1 / attackSpeed / 2);
        pathFinder.enabled = false;
        WeaponController.OnTriggerHold(hitPoint);
        yield return new WaitForSeconds(1 / attackSpeed / 2);
        WeaponController.OnTriggerRelease();

        skinMaterial.color = originalColour;

        nextAttackTime = Time.time + 1 / attackSpeed;
        isAttacking = false;

    }
}
